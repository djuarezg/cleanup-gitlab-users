import os
import requests
import traceback
import logging
import sys
import argparse
import boto3
from botocore.client import Config

parser = argparse.ArgumentParser()
parser.add_argument("-s3url", "--s3_url", required=False)
parser.add_argument("-s3key", "--s3_access_key", required=False)
parser.add_argument("-s3secret", "--s3_secret_access_key", required=False)
parser.add_argument("-s3bucket", "--s3_bucket", required=False)
parser.add_argument("-logfilepath", "--log_file_path", required=False)
parser.add_argument("-logfilename", "--log_file_name", required=False)
args = parser.parse_args()

# Force logger to print through stdout and add default format to messages
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# Retrieve S3 variables, if they exist
s3_url = args.s3_url
s3_access_key = args.s3_access_key
s3_secret_access_key = args.s3_secret_access_key
s3_bucket = args.s3_bucket
log_file_path = args.log_file_path
log_file_name = args.log_file_name

# We allow optional S3 backup as we want to ease tests of this script on https://gitlab.cern.ch/vcs/cleanup-gitlab-users
if s3_url == None or s3_access_key == None or s3_secret_access_key == None or s3_bucket == None or log_file_name == None or log_file_path == None:
  logger.warn("[user_cleanup] S3 credentials are missing, it will omit the S3 backup")
else:
  # Finally, upload the results to S3 as for future reference
  logger.info("[user_cleanup] Uploading logging results to S3 bucket [%s]", s3_bucket)
  s3 = boto3.resource('s3',
                      endpoint_url=s3_url,
                      aws_access_key_id=s3_access_key,
                      aws_secret_access_key=s3_secret_access_key,
                      config=Config(signature_version='s3v4'),
                      region_name='us-east-1')
  
  s3.Bucket(s3_bucket).upload_file(log_file_path, "gitlab_user_cleanup_logs/" + log_file_name)

logger.info("[user_cleanup] GitLab user cleanup finished")