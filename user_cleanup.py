import json
import os
import re
import time
import requests
import traceback
import logging
import sys
import argparse
from ldap3 import Server, Connection, ALL_ATTRIBUTES
from ldap3.core.exceptions import LDAPBindError, LDAPException

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--base_url", required=True)
parser.add_argument("-t", "--admin_token", required=True)
parser.add_argument("-d", "--dry_run", required=False, default='true')
parser.add_argument("-id", "--gitlab_user_id_test", required=False)
args = parser.parse_args()

# Global variables
api_sudo_token = {"Private-Token": args.admin_token}
base_url = args.base_url

# Since we do not want to authenticate, we use an anonymous server connection to xldap
server = Server("xldap.cern.ch")
base_dn = "OU=Users,OU=Organic Units,DC=cern,DC=ch"

# Force logger to print through stdout and add default format to messages
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# Because of users leaving CERN, we have to perform some cleanup on the projects they were handling.
# Users are blocked after they leave the CERN due to their identities. We cannot delete blocked accounts because deleting (destroy) a user also removes all contributions such as comments (see warning when destroying a user from the admin area).
# We'll want to keep issues and comments left by a user after he/she leaves cern, so we can't destroy the Gitlab account if there is such data left behind. 

# Enumerate expired accounts to be processed. Expired accounts will necessarily be Blocked in GitLab (but not all Blocked will be expired). 
# Archive all personal projects. It seems easier to simply use the Archive function in GitLab, as projects can be recovered without restoring from backup (we can always delete later). 
# Mark all personal projects Archived.
# Reset email to account_name@cern.ch. The account name cannot be reused by someone else (or the same person later) so there cannot be conflicts but the email address can be reused by a new user. And GitLab will check for email address conflicts.
# Remove the GitLab user from all Groups and Projects he's member of. If the user is the last owner of a Group, we need to find a new owner before we can remove the last owner (TODO)
# To know an expired account has been processed we will remove its LDAP/SAML identities after cleanup is done so it is not processed again.


#Retrieving the argument and having true as default for Dry-run
# We always convert the argument to a boolean in case script argument is true and false instead of True and False
dryrun = json.loads(args.dry_run.lower())
if dryrun == None:
  logger.info("[user_cleanup DRY-RUN: True] DRY_RUN argument is not set, continuing in dryrun mode.")
  dryrun = True

gitlab_user_id = args.gitlab_user_id_test
if gitlab_user_id == None:
  logger.info("[user_cleanup DRY-RUN: %s] GITLAB_USER_ID argument is not present, continuing without gitlab_user_id mode.", str(dryrun))
else:
  logger.info("[user_cleanup DRY-RUN: %s gitlab_user_id: %s] GITLAB_USER_ID argument exists, running tests.", str(dryrun), str(gitlab_user_id))

# WIP: Currently we have no way of knowing 100% sure who to reassign the ownership to. LDAP supervisor can be a person from a club after a user leaves,
# meaning we cannot automatically reassign.
def get_new_owner(group):
  # Produce the user JSON as retrieved through the Gitlab API and return it.
  return None

def cleanup_user(user, dryrun):
  logger.info("[user_cleanup DRY-RUN: %s] Cleaning up user: %s", str(dryrun), user['username'])
  identity = [identity for identity in user['identities'] if re.match('.*ldap.*', identity['provider'])]
  # In order to use the configured LDAP provider we need credentials, we use an anonymous one.
  extern_uid = identity[0]['extern_uid']
  match = re.search(r'(?<=cn\=)(.+?)(?=\,)', extern_uid)
  if match != None:
    cn = match.group()
  else:
    cn = user['username'] + '_deleted_from_ad'
  username = user['username']
  conn = Connection(server, auto_bind=True)
  search_filter = "(CN=" + cn + ")"
  conn.search(search_base=base_dn, search_filter=search_filter, attributes=ALL_ATTRIBUTES)
  if conn.entries != None and len(conn.entries) > 0:
    # LDAP Search found an entry. Account must not be cleaned up.
    logger.info("[user_cleanup DRY-RUN: %s] LDAP Search found an entry. Account must not be cleaned up.", str(dryrun))
    return
  # We only process users that do not exist in AD anymore
  else:
    # We did not get any result, 
    # We archive all the personal projects
    # First we retrieve all the explicitly owned projects, i.e. Personal projects: /users/:id:/projects?owned=true. Do not confuse with /projects?owned=true while using Sudo
    resp = requests.get(base_url + "/users/" + username + "/projects", data={'owned': 'true'}, headers=api_sudo_token)
    json_personal_projects = resp.json()
    # It needs to be a personal project, so it needs the same namespace as the username. There is no way to retrieve personal projects through API
    identity = [identity for identity in user['identities'] if re.match('.*ldap.*', identity['provider'])]
    for personal_project in json_personal_projects:
      logger.info("[user_cleanup DRY-RUN: %s] Archiving personal project \"%s\"[%s]", str(dryrun), personal_project['path_with_namespace'], str(personal_project['id']))
      if (not dryrun):
        # Just archive the project
        resp = requests.post(base_url+ "/projects/" + str(personal_project['id']) + "/archive", headers=api_sudo_token)

    #We reset the email to account_name@cern.ch (See VCS 698)
    logger.info("[user_cleanup DRY-RUN: %s] User \"%s\"[%s] email changed to \"%s@cern.ch\"; (PREVIOUS primary_email: \"%s\")", str(dryrun), username, str(user['id']), username, user['email'])
    if (not dryrun):
      resp = requests.put(base_url + "/users/" + str(user['id']), data={'email':username+'@cern.ch', 'skip_reconfirmation':'true'}, headers=api_sudo_token)
      # Changing the primary email adds it to the secondary email list, so we need to remove them as well in a second operation
      resp = requests.get(base_url  + "/users/" + str(user['id']) + "/emails", headers=api_sudo_token)
      json_secondary_emails = resp.json()
      for email in json_secondary_emails:
        resp = requests.delete(base_url  + "/users/" + str(user['id']) + "/emails/" + str(email['id']), headers=api_sudo_token)

    solo_owned_groups = []

    # We remove the user from all projects he is member of.
    api_sudo_as = dict(api_sudo_token)
    # Sudo is needed to impersonate the authenticated user for the request
    api_sudo_as["Sudo"] = username
    # There is no way to get just the projects a user is member of, as this will retrieve projects which the user has access to due to groups. Attention: Group members != Project members
    resp = requests.get(base_url  + "/projects", data={'membership':'true'}, headers=api_sudo_as)
    json_belonging_projects = resp.json()
    for belonging_project in json_belonging_projects:
      # We do not care if it is the last project owner, API takes care of not deleting if it is the last owner.
      # We need to know whether the user is a member of the project directly and not through a group
      resp = requests.get(base_url  + "/projects/" + str(belonging_project['id']) + "/members/" + str(user['id']), headers=api_sudo_token)
      json_member = resp.json()
      if (not 'error' in json_member.keys()) and ('id' in json_member.keys()):
        logger.info("[user_cleanup DRY-RUN: %s] Removing user \"%s\"[%s] from project \"%s\"[%s]; (PREVIOUS access_level: \"%s\")", str(dryrun), username, str(user['id']), belonging_project['path_with_namespace'], str(belonging_project['id']), json_member['access_level'])
        if (not dryrun):
          resp = requests.delete(base_url + "/projects/" + str(belonging_project['id']) + "/members/" + str(user['id']), headers=api_sudo_token)

    # There is no way to get just the groups a user is member of, as this will retrieve groups which the user has access to due to projects he is member of. Attention: Group members != Project members
    resp = requests.get(base_url  + "/groups", headers=api_sudo_as)
    json_belonging_groups = resp.json()
    for belonging_group in json_belonging_groups:
      # We do not care if it is the last group owner, API takes care of not deleting if it is the last owner.
      # We need to know whether the user is a member of the project directly and not through a group
      resp=requests.get(base_url  + "/groups/" + str(belonging_group['id']) + "/members/" + str(user['id']), headers=api_sudo_token)
      json_member = resp.json()
      if (not 'error' in json_member.keys()) and ('id' in json_member.keys()):
        logger.info("[user_cleanup DRY-RUN: %s] Removing user \"%s\"[%s] from group \"%s\"[%s]; (PREVIOUS access_level: \"%s\")", str(dryrun), username, str(user['id']), belonging_group['full_path'], str(belonging_group['id']), json_member['access_level'])
        if (not dryrun):
          resp = requests.delete(base_url + "/groups/" + str(belonging_group['id']) + "/members/" + str(user['id']), headers=api_sudo_token)
          try:
            response_json = resp.json()
            # If the user could not abbandon the group because of a Forbidden error.
            if response_json['message'] != None and '403' in response_json['message']:
              solo_owned_groups.append(belonging_group)
          except ValueError:
            # no JSON returned, deletion was successful
            pass
    
    still_solo_owned_groups = False
    # If there is any group that the user could not leave because he is the last owner of.
    if len(solo_owned_groups) > 0:
      # These groups have to be reassigned
      for solo_owned_group in solo_owned_groups:
        new_owner = get_new_owner(solo_owned_group)
        if new_owner != None:
          logger.info("[user_cleanup DRY-RUN: %s] Reassigning group \"%s\"[%s] from \"%s\"[%s] to user \"%s\"[%s]", str(dryrun),solo_owned_group['full_path'], str(belonging_group['id']), username, str(user['id']), new_owner['username'], str(new_owner['id']))
          if (not dryrun):
            # Adding the new owner
            resp = requests.post(base_url + "/groups/" + str(solo_owned_group['id']) + "/members", data={'user_id':str(new_owner['id']),'access_level':'50'}, headers=api_sudo_token)
            # Removing current user as group now has a new owner
            resp = requests.delete(base_url + "/groups/" + str(solo_owned_group['id']) + "/members/" + str(user['id']), headers=api_sudo_token)
        else:
          logger.info("[user_cleanup DRY-RUN: %s] This group requires manual group reassignment [Currently belonging to user: \"%s\"[%s] Group path: \"%s\"[%s]]", str(dryrun), username, str(user['id']),solo_owned_group['full_path'], str(belonging_group['id']))
          still_solo_owned_groups = True

    # We delete the user identities (WIP: WE ONLY REMOVE IDENTITIES WHEN THE COMPLETE GROUP REASSIGNEMENT HAS BEEN DONE)
    if not still_solo_owned_groups:
      logger.info("[user_cleanup DRY-RUN: %s] Cleanup for user finished, deleting identities: \"%s\"[%s] (PREVIOUS identities: \"%s\")", str(dryrun), username, str(user['id']), str(user['identities']))
      if (not dryrun):
        # Identities cannot be deleted, we overwrite them https://gitlab.com/gitlab-org/gitlab-ce/issues/48739 with empty values
        for identity in user['identities']:
          resp = requests.put(base_url + "/users/" + str(user['id']), data={'provider':identity['provider'], 'extern_uid':''}, headers=api_sudo_token)

# MAIN
# Retrieving the first 100 blocked accounts, just to get the total of pages. 100 is the max per page
resp = requests.get(base_url+"/users?blocked=true&per_page=100", headers=api_sudo_token)
# Total pages using 100 per page
total_pages = resp.headers['X-Total-Pages']
# Where we will store all the blocked users. We must retrieve all of them BEFORE cleaning up.
json_blocked_users = []

for page in range(1, int(total_pages) + 1):
  resp = requests.get(base_url + "/users?blocked=true&per_page=100&page=" + str(page), headers=api_sudo_token)
  json_blocked_users_page = resp.json()
  json_blocked_users = json_blocked_users + json_blocked_users_page

# If we got a valid API token we get a list, otherwise we get a JSON object
if type(json_blocked_users) is list:
  # We do not need to process blocked accounts whose identities have been deleted, since this means
  # they have been fully retired already by a previous run of this script.
  # NB: as of GitLab 12.0 (ref. https://gitlab.com/gitlab-org/gitlab-ce/issues/48739) we cannot fully delete identities from the API
  # so "retired" users may still have identities, but all these remaining identities have an empty `extern_uid`.
  # The accounts we still need to process are thus any account having an identity with a non-empty `extern_uid`
  json_blocked_users = [user for user in json_blocked_users if any(identity['extern_uid'] != '' for identity in user['identities']) ]
  for user in json_blocked_users:
    # We skip all users if gitlab_user_id has been sent as an argument
    if gitlab_user_id != None and str(user['id']) != gitlab_user_id:
      continue
    try:
      cleanup_user(user, dryrun)
    except LDAPBindError as ex:
      # We couldn't bind, we skip the account
      logger.warn("[user_cleanup DRY-RUN: %s] Authentication failed: \"%s\"[%s] Exception %s", str(dryrun), user['username'], str(user['id']), ex, exc_info=True)
    except LDAPException as ex:
      logger.warn("[user_cleanup DRY-RUN: %s] LDAP search error: \"%s\"[%s] Exception %s", str(dryrun), user['username'], str(user['id']), ex, exc_info=True)
    except Exception as ex:
      logger.warn("[user_cleanup DRY-RUN: %s] Cleanup failure: \"%s\"[%s] Exception %s", str(dryrun), user['username'], str(user['id']), ex, exc_info=True)
else:
  logger.warn("[user_cleanup DRY-RUN: %s] API token is not valid", str(dryrun))