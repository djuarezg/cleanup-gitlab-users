# make sure to fail on any error
set -e

# helper functions

# polls the specified Jobs API endpoint until a successful job is found
# for the specified commit
function wait_for_ci_pipeline_success {
    local pipelines_api_url=$1
    local sha=$2
    # time out after 10 min
    local timeout=600
    local check_end=$[ $SECONDS + $timeout ]
    local check_interval=10

    while [ $SECONDS -lt $check_end ]; do
        pipeline=$(curl --fail --silent -H "Authorization: Bearer ${OAUTH_TOKEN}" -X GET "${pipelines_api_url}" -d status=success | \
            jq --arg sha "${sha}" -r '.[] | select(.sha==$sha)')
        if [ -n "${pipeline}" ]; then
            echo "${pipeline}"
            return 0
        fi
        echo "Waiting for a CI pipeline to succeed for commit ${sha}"
        sleep $check_interval
    done
    echo "No CI pipeline succeeded after $timeout seconds. Pipelines for ${sha} are:"
    curl --fail --silent -H "Authorization: Bearer ${OAUTH_TOKEN}" -X GET "${pipelines_api_url}" | \
        jq --arg sha "${sha}" -r '.[] | select(.sha==$sha)'
    return 1
}
# from a git repo URL and a folder name, clones and pushes something into a git repo
function git_clone_push {
    local git_url=$1
    local folder=$2

    git clone "${git_url}" "${folder}"
    (cd "${folder}" && echo "$(pwd)" >> myfile && git add myfile && git commit -m "commit from $(pwd)" && git push origin master)
}
# from a git repo URL and a folder name, clones and pushes something into a git repo using LFS
function git_clone_push_lfs {
    local git_url=$1
    local folder=$2

    git clone "${git_url}" "${folder}"
    (cd "${folder}" && git lfs install && git lfs track "*.lfs" && echo "$(pwd)" >> myfile.lfs && git add myfile.lfs && git commit -m "commit from $(pwd) using LFS" && git push origin master)
}
# obtain API Oauth token from username/password as per https://docs.gitlab.com/ce/api/oauth2.html#1-requesting-access-token
function api_login {
    OAUTH_TOKEN=$(curl --fail --silent -X POST "${TEST_GITLAB_SERVER}/oauth/token" -d grant_type=password -d username="${TEST_GITLAB_USER}" -d password="${TEST_GITLAB_PASSWORD}" | jq -r .access_token)
}

function log {
    echo "[$(date -R)] $@"
}