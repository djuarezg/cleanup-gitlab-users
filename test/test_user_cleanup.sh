## part 3: test user cleanup
source test/common.sh
echo "${TEST_GITLAB_PASSWORD}" | kinit -f "${TEST_GITLAB_USER}@CERN.CH" >/dev/null
api_login

# We recreate the test user so we control memberships from scratch independently from other tests
log 'delete our test user from gitlab-dev so we can check user creation (using admin user)'
# NB: we need to start with this because the previous tests typically leave the test user in a "retired" state
set +e
# NB: we need to start with this because the previous tests typically leave the test user in a "retired" state
OLD_USER_ID=$(curl --fail --silent --show-error -H "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/users" -d username="${TEST_GITLAB_USER}" | jq -r ".[].id")
if [ -n "$OLD_USER_ID" ]; then
    curl --fail --silent -H "Authorization: Bearer ${ADMIN_TOKEN}" -X DELETE "${TEST_GITLAB_SERVER}/api/v4/users/${OLD_USER_ID}"
fi
set -e

log 'Verify that a SSO login re-creates the user and obtain a new API token'
# Deletion is asynchronous as per https://gitlab.com/gitlab-org/gitlab-ce/issues/31750 so we need to wait for user to be actually deleted!
sleep 60s && cern-get-sso-cookie --krb --url "${TEST_GITLAB_SERVER}" --outfile sso.cookie && grep _gitlab_session sso.cookie >/dev/null
sleep 10s && api_login
NEW_USER_ID=$(curl --fail --silent -H "Authorization: Bearer ${OAUTH_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/user" | jq -r .id)
# make sure user was actually deleted and re-created. If not, something could prevent deletion of the user (e.g. last owner of a group)
# and the test for user re-creation is not valid!
if [ -n "$OLD_USER_ID" ]; then
    test "${NEW_USER_ID}" -ne "${OLD_USER_ID}"
fi


log 'Verify that we have all 3 identities for the re-created user (using admin user)'
identities=$(curl --fail --silent -H "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/users" -d username="${TEST_GITLAB_USER}" | jq -r ".[].identities[].provider")
echo "$identities" | grep kerberos > /dev/null && echo "$identities" | grep ldap > /dev/null && echo "$identities" | grep saml > /dev/null

log 'Get test group for the test'
TEST_PROJECT_NAME="test-${CI_JOB_ID}"
TEST_GROUP_ID=$(curl --fail --silent -H "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/groups/${TEST_GITLAB_GROUP}" | jq -r .id)
log 'Add the user to the TEST_GITLAB_GROUP group as owner'
curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X POST "${TEST_GITLAB_SERVER}/api/v4/groups/${TEST_GROUP_ID}/members" -d user_id=${NEW_USER_ID} -d access_level=50
log 'Create a new project on the group so it inherits the group memberships'
TEST_PROJECT_ID=$(curl --fail --silent -H "Authorization: Bearer ${ADMIN_TOKEN}" -X POST "${TEST_GITLAB_SERVER}/api/v4/projects" -d name="${TEST_PROJECT_NAME}" -d namespace_id="${TEST_GROUP_ID}" | jq -r .id)
[ -n "${TEST_PROJECT_ID}" ] && echo "Created project ${TEST_GITLAB_GROUP}/${TEST_PROJECT_NAME} with ID ${TEST_PROJECT_ID}"


# Preparation for the user-cleanup
log 'Change the user identities to simulate LDAP blocking'
identities=$(curl -H "Authorization: Bearer ${ADMIN_TOKEN}" -X PUT "${TEST_GITLAB_SERVER}/api/v4/users/${NEW_USER_ID}" -d provider=ldapmain -d extern_uid="cn=myuser_deleted_from_ad,ou=users,ou=organic units,dc=cern,dc=ch")

# Force user LDAP sync
log 'Temporarily change the email so it does not get back all the original identities and force LDAP sync on the test user, then remove all identities so API can update the email'
pod=$(oc -n gitlab get pods -l deploymentconfig=gitlab-web -o jsonpath="{.items[0].metadata.name}")
# We force the email update through Rails to avoid https://gitlab.com/gitlab-org/gitlab-ce/issues/17797 on the test account. This is required for the LDAP sync to block the account. This is not required for real accounts.
# We remove the identities and add them back because otherwise any API call that tries to modify the email will not work.
ldap_sync_script=$(echo "user = User.find_by(username: '${TEST_GITLAB_USER}'); user.email = 'temp_email@cern.ch'; user.save; Gitlab::Auth::LDAP::Access.allowed?(User.find_by(username: '${TEST_GITLAB_USER}')); user.identities = []; user.save;")
oc exec $pod gitlab-rails runner "${ldap_sync_script}"

# Restore the ldap identity, otherwise cleanup will not deal with this account
log 'Restore the ldap identity for the cleanup script'
identities=$(curl -H "Authorization: Bearer ${ADMIN_TOKEN}" -X PUT "${TEST_GITLAB_SERVER}/api/v4/users/${NEW_USER_ID}" -d provider=ldapmain -d extern_uid="cn=myuser_deleted_from_ad,ou=users,ou=organic units,dc=cern,dc=ch")


# Preparatory steps for the cleanup script
log 'Create a personal project for the user'
personal_project=$(curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X POST "${TEST_GITLAB_SERVER}/api/v4/projects/user/${NEW_USER_ID}" -d name="personal_project-${TEST_PROJECT_NAME}" -d visibility=public | jq ".id")

log 'Have a different email on the user'
curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X PUT "${TEST_GITLAB_SERVER}/api/v4/users/${NEW_USER_ID}" -d email="test.gitlab.user@cern.ch" -d skip_reconfirmation=true
# Get the ID from another user such as the admin test user.
admin_id=$(curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/users" -d username=${TEST_GITLAB_ADMIN_USER} | jq ".[0].id" )

log 'Add the user as a member to a project where he would be able to leave. Owner will be inherited'
curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X POST "${TEST_GITLAB_SERVER}/api/v4/projects/${TEST_PROJECT_ID}/members" -d user_id=${NEW_USER_ID} -d access_level=10

log 'Extract the cronjob definition using Openshift REST API to create a job'
test_user_cleanup=$(oc get cronjob/cleanup-ex-cern-users -o json | jq '{ "apiVersion": "batch/v1", "kind": "Job", "spec": .spec.jobTemplate.spec, "metadata": (.spec.jobTemplate.metadata * { "name": (.metadata.name + "-test"), "ownerReferences": [ { "apiVersion": .apiVersion, "kind": .kind, "name": .metadata.name, "uid": .metadata.uid } ] }) }')

log 'Replace the job definition with the GITLAB_USER_ID to run the cleanup on'
test_user_cleanup=$(echo "$test_user_cleanup" | jq --arg NEW_USER_ID "${NEW_USER_ID}" "(.spec.template.spec.containers[0].env | .[] | select(.name==\"GITLAB_USER_ID\") | .value) |= \"$NEW_USER_ID\"")
echo $test_user_cleanup
# Set DRY-RUN to false for the dev instance
DRY_RUN=false
test_user_cleanup=$(echo "$test_user_cleanup" | jq --arg DRY_RUN "${DRY_RUN}" "(.spec.template.spec.containers[0].env | .[] | select(.name==\"DRY_RUN\") | .value) |= \"$DRY_RUN\"")
echo $test_user_cleanup
log 'Start the job run replacing any old job. Since there is no replace option, we have to delete and create it'
set +e
oc delete job cleanup-ex-cern-users-test >/dev/null
set -e
echo $test_user_cleanup | oc create -f -
log 'Waiting for the job...'
sleep 60s

log 'Check that the personal project created before is archived'
result=$(curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/projects/${personal_project}" | jq "(.archived == true)")
test "${result}" == "true"
log 'Check that the user email changed'
result=$(curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/users/${NEW_USER_ID}" | jq --arg TEST_GITLAB_USER "$TEST_GITLAB_USER" ".email == \"${TEST_GITLAB_USER}@cern.ch\"")
test "${result}" == "true"
log 'Check that the user is not an owner of the group anymore'
result=$(curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/groups/${TEST_GROUP_ID}/members" )
echo $result
result=$(curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/groups/${TEST_GROUP_ID}/members" | jq --arg TEST_GITLAB_USER "$TEST_GITLAB_USER" "length == 1 and ( .[] | .username != \"${TEST_GITLAB_USER}\")")
test "${result}" == "true"
log 'Check that the user left the TEST_PROJECT'
result=$(curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/projects/${TEST_PROJECT_ID}/members" | jq "length == 0")
test "${result}" == "true"
log 'Check that the identities were deleted'
result=$(curl --header "Authorization: Bearer ${ADMIN_TOKEN}" -X GET "${TEST_GITLAB_SERVER}/api/v4/users/${NEW_USER_ID}" | jq -e ".identities | [.[].extern_uid == \"\"] | any")
test "${result}" == "true"
