# User cleanup

This project solves the [account problems with users leaving CERN](https://its.cern.ch/jira/browse/VCS-330).
It manages the group and project membership, identities and email so if the user returns to CERN there will be no email conflict that ServiceDesk support has to deal with.

## Requirements

The script needs a GitLab [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with `sudo` privileges.
See https://cern.ch/gitlabdocs/deployment/resources/#service-accounts for the service account used for this (we can use the same service account for all GitLab instances,
but it will have a different PAT in each instance)

## Cleanup logic

Because of users leaving CERN, we have to perform some cleanup on the projects they were handling.
Users are blocked after they leave the CERN due to their identities. We cannot delete blocked accounts because deleting (destroy) a user also removes all contributions such as comments (see warning when destroying a user from the admin area).
We'll want to keep issues and comments left by a user after he/she leaves cern, so we can't destroy the Gitlab account if there is such data left behind. 

The script logic is as it follows:
* Enumerate expired accounts to be processed. Expired accounts will necessarily be Blocked in GitLab (but not all Blocked will be expired). 
* Archive all personal projects. It seems easier to simply use the Archive function in GitLab, as projects can be recovered without restoring from backup (we can always delete later). 
* Reset email to account_name@cern.ch. The account name cannot be reused by someone else (or the same person later) so there cannot be conflicts but the email address can be reused by a new user. GitLab will check for email address conflicts.
* Remove the GitLab user from all Groups and Projects he's member of. If the user is the last owner of a Group, we need to find a new owner before we can remove the last owner (TODO). Print on the logs if this was not possible for manual reassignation. This can be seen on the Kibana logs.
* Once an expired account has been fully retired, we will remove all its identities (LDAP/SAML/Kerberos).
  This allows the next run to identify accounts that do not need any further processing.
  **Important note:** as of GitLab 12.0, we [cannot delete identities via the API](https://gitlab.com/gitlab-org/gitlab-ce/issues/48739). As a workaround, we keep identities and set `extern_uid` to an empty string.
  TODO: when identities can be deleted via the API, do that and delete any old identity with empty string as `extern_uid`.
* Logging: activity and changes performed by the script are printed to STDOUT
  (will be sent to Central Monitoring via general Openshift container logging),
  and also (for archival purpose) in log files the `gitlaboperations(-dev)` S3 bucket.

Since we do not have a reliable way of knowing to whom we should reassign groups where the blocked user is the last owner of, these groups will require manual reassignment. **You can identify these groups by looking for `[user_cleanup DRY-RUN: true/false] This group requires manual group reassignment` on Kibana logs or looking for the past logs on the `gitlaboperations(-dev) bucket.**

## Configure CI secret variables for test scripts

This project uses `testgitlabadm` and `testgitlab` service accounts as <https://gitlab.cern.ch/vcs/oo-gitlab-cern> for its tests. This implies that the following CI/CD variables are required:

* TEST_GITLAB_ADMIN_USER
* TEST_GITLAB_ADMIN_PASSWORD
* TEST_GITLAB_USER
* TEST_GITLAB_PASSWORD

For the tests we do not use the S3 backup of the log results.

Tests also need the Openshift serviceaccount `gitlab-operations` created by [oo-gitlab-cern](https://gitlab.cern.ch/vcs/oo-gitlab-cern), as part of the GitLab deployment.

Obtain gitlab's dev `gitlab-operations` token with:
```bash
oc login openshift-dev.cern.ch
oc serviceaccounts get-token gitlab-operations -n gitlab
```

And add it to this project's secret variables using the following variable name: `GITLAB_OPERATIONS_TOKEN_DEV`.
